# Scala project

## Description

This scala project has been done by H�l�ne Ksiezak and Florian Lapeyre.

The three csv files are parsed and stored in different LinkedHashMap.

As **bonus**, we handled the case when the user passes a partial country name
as input, checking if it is a substring of an existing country name.

## Usage

To start the program, run the following command at the root
  > 'sbt run'

In the main menu, type '1' or 'Query' to enter a **query**.
Type '2' or 'Reports' to get a **report**. Type '3' to **exit**.

### Query

Type the country code, name, or a part of its name to
have informations about a country.

### Reports

Type '1', '2' or '3' to the have corresponding report as explained in the menu.

### Tests

To start the tests, run the following command at the root
  > 'sbt test'
