import org.scalatest._

import scala.collection.mutable.LinkedHashMap

class ParserSpec extends FlatSpec with Matchers{

  behavior of "readFile"

  it should "return a List[String]" in {
    val readLines = Parser.readFile("resources/test/empty.csv")
    readLines should be (List[String]())
  }
  
  it should "read all lines of a file" in {
    val readLines = Parser.readFile("resources/test/lines.csv")
    readLines should be (List("\"id\", \"code\", \"name\"", "123, \"FR\", \"France\""))
  }

  it should "read all lines even if there is only one" in {
    val readLines = Parser.readFile("resources/test/oneLine.csv")
    readLines should be (List("bonjour"))
  }

  it should "throw ParseException if a file is not found" in {
    an [ParseException] should be thrownBy Parser.readFile("resources/test/notExisting.csv")
  }

  ignore should "throw ParseException if a file that cannot be read" in {
    an [ParseException] should be thrownBy Parser.readFile("resources/test/nonReadable.csv")
  }

  it should "read all lines of a file that cannot be written, but can be read" in {
    noException should be thrownBy Parser.readFile("resources/test/nonWritable.csv")
  }



  behavior of "getFields"

  it should "parse simple fields" in {
    val line = Parser.getFields("\"id\",\"name\",\"code\",\"continent\"")
    line should be (List("id", "name", "code", "continent"))
  }

  it should "parse unique field" in {
    val line = Parser.getFields("\"id\"")
    line should be (List("id"))
  }

  it should "parse fields containing comas" in {
    val line = Parser.getFields("\"id\",\"na,me\",\"code,\",\",continent\"")
    line should be (List("id", "na,me", "code,", ",continent"))
  }

  it should "throw ParseException if it is not valid (quote not closed)" in {
    an [ParseException] should be thrownBy Parser.getFields("\"id\",\"name\",\"code\",\"continent")
    an [ParseException] should be thrownBy Parser.getFields("\"id,\"name\",\"code\",\"continent")
    an [ParseException] should be thrownBy Parser.getFields("\"id\",\"name\",\"code\",\"conti\",\"nent")
    an [ParseException] should be thrownBy Parser.getFields("\"id\",\"name\",\"code\",\"continent\"\"")
  }

  it should "throw ParseException if it is an id isn't sourrounded by quotes" in {
    an [ParseException] should be thrownBy Parser.getFields("\"id\",\"name\",\"code\",continent")
  }



  behavior of "getNextWord"

  it should "return next word and new line" in {
    val result = Parser.getNextWord("abc,def,ghi")
    result.newLine should be ("def,ghi")
    result.word should be ("abc")
  }

  it should "handle comas in quotes" in {
    val result = Parser.getNextWord("\"a,b\"c,def,ghi")
    result.newLine should be ("def,ghi")
    result.word should be ("\"a,b\"c")
  }

  it should "handle empty words" in {
    val result = Parser.getNextWord(",def,ghi")
    result.newLine should be ("def,ghi")
    result.word should be ("")
  }

  it should "throw ParseException if a string isn't closed" in {
    an [ParseException] should be thrownBy Parser.getNextWord("a\"bc,def,ghi")
  }



  behavior of "parseLine"

  it should "Correctly parse a line" in {
    val map = Parser.parseline("1,2,\"3\"", List("id", "name", "code"))
    map should be (Map("id" -> "1", "name" -> "2", "code" -> "\"3\""))
  }

  it should "work even if there are more elements than fields" in {
    val map = Parser.parseline("1,2,\"3\"", List("id", "name"))
    map should be (Map("id" -> "1", "name" -> "2"))
  }



  behavior of "fuzzingKeyMap"

  it should "Find an element having a key containing the string" in {
    val map = LinkedHashMap("\"Fr\"" -> 0, "Bonjour" -> 1, "testing" -> 2, "Bonjou" -> 3)
    Parser.fuzzingKeyMap(map, "\"Fr\"") should be (Some(0))
    Parser.fuzzingKeyMap(map, "Bonj") should be (Some(3))
    Parser.fuzzingKeyMap(map, "\"nj\"") should be (Some(3))
    Parser.fuzzingKeyMap(map, "\"t\"") should be (Some(2))
    Parser.fuzzingKeyMap(map, "\"Fr\"") should be (Some(0))
  }

  it should "return None when it doesn't find anything" in {
    val map = LinkedHashMap("\"Fr\"" -> 0, "Bonjour" -> 1, "testing" -> 2, "bonjou" -> 3)
    Parser.fuzzingKeyMap(map, "z") should be (None)
    Parser.fuzzingKeyMap(map, "\"z\"") should be (None)
    Parser.fuzzingKeyMap(map, "\"FR\"") should be (None)
    Parser.fuzzingKeyMap(map, "\"bonjour\"") should be (None)
  }



  behavior of "getMapElement"

  it should "return the value when the key is found" in {
    val map = scala.collection.mutable.Map("id" -> 1, "test" -> 2)
    Parser.getMapElement(map, "test", "test") should be (2)
  }

  it should "throw ParseException when key not found" in {
    val map = scala.collection.mutable.Map("id" -> 1, "test" -> 2)
    an [ParseException] should be thrownBy Parser.getMapElement(map, "id2", "test")
  }



  // From there, maps are filled with values contained in real files.
  behavior of "parse_countries"

  it should "associate code to other elements" in {
    Parser.parse
    val us = Parser.countries("\"US\"")
    us("id") should be ("302755")
    us("wikipedia_link") should be ("\"http://en.wikipedia.org/wiki/United_States\"")

    val ru = Parser.countries("\"RU\"")
    ru("keywords") should be ("\"Soviet, Sovietskaya, Sovetskaya\"")
  }

  it should "associate name to code" in {
    Parser.countryNameToCode("\"Belgium\"") should be ("\"BE\"")
    Parser.countryNameToCode("\"Cook Islands\"") should be ("\"CK\"")
    Parser.countryNameToCode("\"Côte d'Ivoire\"") should be ("\"CI\"")
  }



  behavior of "parse_airports"

  it should "associate id to other elements" in {
    val airport1 = Parser.airports("6548")
    airport1("id") should be ("6548")
    airport1("continent") should be ("\"NA\"")
    airport1("iso_region") should be ("\"US-MN\"")

    val airport2 = Parser.airports("23037")
    airport2("ident") should be ("\"NH85\"")
    airport2("type") should be ("\"heliport\"")
    airport2("wikipedia_link") should be ("")
  }

  it should "update the airport list" in {
    val us = Parser.airportsPerCountry("\"US\"")
    us should contain allOf ("7255", "6523", "16030")

    val jp = Parser.airportsPerCountry("\"JP\"")
    jp should contain allOf ("313629", "41849", "313630", "309677")
  }



  behavior of "parse_runways"

  it should "associate id to other elements" in {
    val runway1 = Parser.runways("269446")
    runway1("airport_ref") should be ("6793")
    runway1("surface") should be ("\"TURF\"")
    runway1("le_ident") should be("\"H1\"")
  }
}
