import scala.io.StdIn
import scala.collection.mutable.LinkedHashMap

object Main
{
  def print_country(countryCode : String) : Unit = {
    val country = Parser.countries(countryCode)

    val countryName = Parser.getMapElement(country, "name", "country")
    val countryId = Parser.getMapElement(country, "id", "country")
    val countryContinent = Parser.getMapElement(country, "continent", "country")

    print(countryName + "(" + countryCode + "): id = "
          + countryId + ", continent = "
          + countryContinent)
  }

  def print_airport(airportId : String) : Unit = {
    val airport = Parser.airports(airportId)
    val airportName = Parser.getMapElement(airport, "name", "airport")
    val airportType = Parser.getMapElement(airport, "type", "airport")
    val airportRegion = Parser.getMapElement(airport, "iso_region", "airport")

    print(airportName + ": id = "
          + airportId + ", type = "
          + airportType + ", region = "
          + airportRegion)
  }

  def print_runway(runwayId : String) : Unit = {
    val runway = Parser.runways(runwayId)
    val runwayLength = Parser.getMapElement(runway, "length_ft", "runway")
    val runwayWidth = Parser.getMapElement(runway, "width_ft", "runway")
    val runwaySurface = Parser.getMapElement(runway, "surface", "runway")
    val runwayLatitude = Parser.getMapElement(runway, "le_ident", "runway")

    print(runwayId + ": length = " + runwayLength
             + ", width = " + runwayWidth
             + ", surface = " + runwaySurface
             + ", latitude = " + runwayLatitude)
          
  }

  def process_report_airports_per_country : Unit = {
    def printOneEntry(countryId : String, nbAirports : Int) = {
        print_country(countryId)
        println("\n    ===> " + nbAirports + " airports")
    }

    println("Here are the ten countries with the most airports:\n")
    Parser.airportsPerCountry.take(10).foreach({
      case (countryId, airportList) => printOneEntry(countryId, airportList.size)
    })
    println

    val reversed_airportsPerCountry = LinkedHashMap(Parser.airportsPerCountry.toSeq.reverse:_*)

    println("Here are the ten countries with the most airports:\n")
    reversed_airportsPerCountry.take(10).foreach({
      case (countryId, airportList) => printOneEntry(countryId, airportList.size)
    })
  }
  
  def process_report_types_runways : Unit = {
    println("Here are the different type of runways per country:\n")

    Parser.runwayTypesPerCountry.foreach({
      case (countryCode, runwayList) => {

        println(countryCode + ":")

        if (runwayList.size == 0)
          println("  No Runway in this country")
        runwayList.foreach(runwayType => println(" - " + runwayType))
      }
    })
  }

  def process_report_latitudes_runways : Unit = {
    println("Here are the ten most common runway latitude:\n")

    Parser.runwayLatitudeOccurences.foreach({
      case (latitude, nbOccurences) =>
        println("The latitude " + latitude + " appeared " + nbOccurences + " times.")
    })
  }

  def process_query(country : String) =
  {
    val countryCode = Parser.getCountryCode(country)
    if (!countryCode.isDefined)
      println("The country '" + country + "' has not been found")
    else
    {
      val airports = Parser.airportsPerCountry(countryCode.get)

      print_country(countryCode.get)
      println(":")
      println("  Airports: " + airports.size)

      airports.foreach(airportId => {
        print("    - ")
        print_airport(airportId)
        println(":")

        val runways = Parser.runwaysPerAirports(airportId)

        println("      Runways: " + runways.size)
        runways.foreach(runwayId => {
          print("        - ")
          print_runway(runwayId)
          println
        })
      })
    }
  }

  def display_header : Unit = {
    println("\n========================\n")
  }

  def query_menu : Unit = {
    display_header
    println("Which country do you want to scan ?")
    val userInput = StdIn.readLine
    println("\n")
    process_query("\"" + userInput + "\"")
    menu
  }

  def report_menu : Unit = {
    display_header
    println("Which report do you want ?")
    println("  1: Get the 10 countries with most and less airports")
    println("  2: Get the types of runways per country")
    println("  3: Get the 10 most common runway latitudes")
    val userInput = StdIn.readLine
    println("\n")
    userInput match
    {
      case "1" => process_report_airports_per_country
      case "2" => process_report_types_runways
      case "3" => process_report_latitudes_runways
      case _ => println("Invalid action '" + userInput + "'")
    }
    menu
  }

  def menu : Unit = {
    display_header
    println("Which action do you want to perform ?")
    println("  1: Query")
    println("  2: Reports")
    println("  3: Exit")
    val userInput = StdIn.readLine
    println("\n")
    userInput match {
      case "1" | "Query" => query_menu
      
      case "2" | "Reports" => report_menu

      case "3" | "Exit" => println("GoodBye!")

      case _ => {
        println("Invalid action '" + userInput + "'")
        menu
      }
    }
  }

  def main(args : Array[String]) : Unit =
  {
    try {
      println("Parsing the files ...")
      Parser.parse
      println("Files parsed!")

      menu
    }
    catch {
      case e : ParseException => println("FAILURE: " + e.getMessage)
      case e : Throwable => e.printStackTrace
    }
  }
}
