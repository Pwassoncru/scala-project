import scala.collection.mutable.LinkedHashMap
import scala.collection.mutable.MutableList
import scala.collection.mutable.Set
import scala.io.Source
import java.io.IOException
import java.io.FileNotFoundException

final case class ParseException(private val message : String = "Parsing error",
                                private val cause : Throwable = None.orNull)
                 extends Exception(message, cause)

object Parser
{

  case class Element(newLine : String, word : String)

  def getNextWord(line : String) : Element =
  {
    // We want to check whether the character is inside a string or not, so we
    // recoded the indexOf function.
    def indexOfComa(str : List[Char], inside_string : Boolean, index : Int) : Int =
    {
      (str, inside_string) match
      {
        // csv malformed.
        case (Nil, true) => throw ParseException("A quote is never closed")
        // end of line
        case (Nil, false) => -1
        // character ',' found
        case (head::tail, false) if head == ',' && !inside_string => index
        // entering or leaving a string
        case (head::tail, _) if head == '\"' => indexOfComa(tail, !inside_string, index + 1)
        case (head::tail, _) => indexOfComa(tail, inside_string, index + 1)
      }
    }

    // String is a mutable collection. We could just iterate over it using a for loop,
    // but since it is not allowed, we cast it into a list to recurse over it more easily
    val index = indexOfComa(line.toList, false, 0)

    // Checking end of line
    if (index == -1)
      Element("", line)
    else
      Element(line.substring(index + 1), line.substring(0, index))
  }

  // Returns the list of fields.
  def getFields(line : String) : List[String] =
  {
    def getFieldsRec(line : String, list : List[String]) : List[String] =
    {
      val element = getNextWord(line)
      // Removing the quotes to access elements more easily.
      // It could be done in getNextWord function, but it is fine to have it only
      // for field names.
      if (element.word.length < 2
         || element.word(0) != '\"'
         || (element.word takeRight 1) != "\"")
        throw ParseException("Fields must always be surrounded by quotes")
      val newfield = element.word.substring(1, element.word.length - 1)
      val newlist = newfield::list
      if (element.newLine == "")
        newlist
      else
        getFieldsRec(element.newLine, newlist)
    }
    getFieldsRec(line, Nil).reverse
  }

  // Parse a line of csv, into a map[Field, element]
  def parseline(line : String, fields : List[String]) : LinkedHashMap[String, String] =
  {
    var parsedElements = LinkedHashMap[String, String]()

    def parseEntry(line : String, fields : List[String]) : Unit =
    {
      val element = getNextWord(line)
      val field = fields.head
      val tail = fields.tail

      // Adding the field to the map of the current entry
      parsedElements += (field -> element.word)

      // Recursing
      if (tail != Nil)
        parseEntry(element.newLine, tail)
    }

    parseEntry(line, fields)

    return parsedElements
  }

  def readFile(fileName : String) : List[String] = {
    try {
      val openedFile = Source.fromFile(fileName)
    
      val lines = openedFile.getLines.toList

      openedFile.close

      lines
    }
    catch {
      case e : FileNotFoundException => throw ParseException(e.getMessage)
      case e : IOException => throw ParseException("IOException: " + e.getMessage)
    }
  }

  // Try to find if keyToExpand is a substring of a key of the map
  def fuzzingKeyMap[U](map : LinkedHashMap[String, U], keyToExpand : String) : Option[U] = {

    if (keyToExpand.length < 2)
      None
    else
    {
      val keyToExpandWithoutQuote = keyToExpand.substring(1, keyToExpand.length - 1)

      val foldInit : Option[U] = None
      map.foldLeft(foldInit)((acc, mapEntry) => mapEntry match {
        case (key, value) => {
          if (key contains keyToExpandWithoutQuote)
            Some(value)
          else
            acc
        }
      })
    }
  }

  def getCountryCode(countryReference : String) : Option[String] = {
    val countryCode = countries.get(countryReference);

    // countryReference is a country code
    if (countryCode.isDefined)
      Some(countryReference)
    else {
      val countryCodeFromName = countryNameToCode.get(countryReference)
      // countryReference is country name
      if (countryCodeFromName.isDefined)
        countryCodeFromName
      // Taking a country name containing the countryReference
      else
        fuzzingKeyMap(countryNameToCode, countryReference)
    }
  }

  // Used to factorize the throw of the exception when key not found
  def getMapElement[U] (map : scala.collection.mutable.Map[String, U],
                        key : String,
                        mapName : String) : U = {
    map.getOrElse(key, throw ParseException("Key not found in the "
                                            + mapName + "table: " + key))
  }

  def parse_countries : Unit = {

    val lines = readFile("resources/main/countries.csv")

    // Getting fields, written on the first line of the file
    val fields = getFields(lines.head)

    lines.tail.foreach(line => {
      val parsedLine = parseline(line, fields)

      val countryCode = getMapElement(parsedLine, "code", "country")
      val countryName = getMapElement(parsedLine, "name", "country")

      countries += (countryCode -> parsedLine)
      countryNameToCode += (countryName -> countryCode)
      airportsPerCountry += (countryCode -> MutableList[String]())
      runwayTypesPerCountry += (countryCode -> Set[String]())
    })
  }

  def parse_airports : Unit = {
    val lines = readFile("resources/main/airports.csv")

    // Getting fields, written on the first line of the file
    val fields = getFields(lines.head)

    lines.tail.foreach(line => {
      val parsedLine = parseline(line, fields)
      val airportId = getMapElement(parsedLine, "id", "airport")
      val countryCode = getMapElement(parsedLine, "iso_country", "airport")
      val airportsList = airportsPerCountry.getOrElse(countryCode,
                         throw ParseException("Country not found: " + countryCode))

      airports += (airportId -> parsedLine)
      airportsList += airportId
      runwaysPerAirports += (airportId -> MutableList[String]())
    })
  }

  def parse_runways : Unit = {
    val lines = readFile("resources/main/runways.csv")

    // Getting fields, written on the first line of the file
    val fields = getFields(lines.head)

    lines.tail.foreach(line => {
      val parsedLine = parseline(line, fields)
      val airportId = getMapElement(parsedLine, "airport_ref", "airport")
      val runwayId = getMapElement(parsedLine, "id", "runway")
      val runwayType = getMapElement(parsedLine, "surface", "runway")
      val runwayLatitude = getMapElement(parsedLine, "le_ident", "runway")
      val airport = airports.getOrElse(airportId,
                    throw ParseException("Airport not found: " + airportId))
      val countryCode = getMapElement(airport, "iso_country", "airport")

      runways += (runwayId -> parsedLine)
      runwaysPerAirports(airportId) += runwayId
      if (runwayType != "")
        runwayTypesPerCountry(countryCode) += runwayType
      if (runwayLatitudeOccurences isDefinedAt runwayLatitude)
        runwayLatitudeOccurences(runwayLatitude) += 1
      else
        runwayLatitudeOccurences += (runwayLatitude -> 1)
    })
  }

  def parse : Unit = {
    parse_countries

    parse_airports

    parse_runways

    // Sorting so that latitudes with most occurences are first.
    runwayLatitudeOccurences = LinkedHashMap(runwayLatitudeOccurences.toSeq.sortWith(_._2 > _._2):_*).take(10)

    // Sorting so that countries with most airports are first
    airportsPerCountry = LinkedHashMap(airportsPerCountry.toSeq.sortWith(_._2.size > _._2.size):_*)
  }

  // map (countryCode -> Map(Field -> element)
  var countries = LinkedHashMap[String, LinkedHashMap[String, String]]()
  // map (airportId -> Map(Field -> element)
  var airports = LinkedHashMap[String, LinkedHashMap[String, String]]()
  // map (runwayId -> Map(Field -> element)
  var runways = LinkedHashMap[String, LinkedHashMap[String, String]]()

  // Map(CountryCode -> MutableList(airportId))
  var airportsPerCountry = LinkedHashMap[String, MutableList[String]]()
  // Map(CoutnryCode -> Set(runwayType))
  var runwayTypesPerCountry = LinkedHashMap[String, Set[String]]()
  // Map(airportId -> MutableList(runwayId))
  var runwaysPerAirports = LinkedHashMap[String, MutableList[String]]()
  // Map(runwayLatitude -> nbOccurences)
  var runwayLatitudeOccurences = LinkedHashMap[String, Int]()
  // Map(countryName -> countryCode)
  var countryNameToCode = LinkedHashMap[String, String]()
}
